import * as Blockly from 'blockly/core';

const ACTUATOR_HUE = 100;

/**
 * Menu 菜单选项
 */

//LED灯灯泡菜单的选项
export const number = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown([
                ["1", "0"],
                ["2", "1"],
                ["3", "2"],
                ["4", "3"]
            ]), 'op')
        this.setOutput(true);
    }
};

/**
 * LED灯颜色菜单选项
 */
export const menu_icb_led_hue = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        var dd = new Blockly.FieldDropdown([
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_CLOSED, "0"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_RED, "1"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_ORANGE, "2"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_YELLOW, "3"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_GREEN, "4"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_CYAN, "5"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_BLUE, "6"],
            [Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE_PURPLE, "7"]
        ]);
        dd.setValue('1');
        this.appendDummyInput("")
            .appendField(dd, "flag");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_LED_HUE);
    }
};

/**
 * 设置内嵌LED灯的颜色
 */
export const icb_actuator_led_hue = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_SETTING);
        this.appendValueInput('led')
            .appendField(Blockly.Msg.MIXLY_BUILDIN_LED);
        this.appendValueInput('hue')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_HUE);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_LED_HUE);
    }
};

/**
 * 红外线信号菜单选项
 */
export const menu_icb_infrared_signal = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        var dd = new Blockly.FieldDropdown([
            ['0', "0"],
            ['1', "1"],
            ['2', "2"],
            ['3', "3"],
            ['4', "4"],
            ['5', "5"],
            ['6', "6"],
            ['7', "7"],
            ['8', "8"],
            ['9', "9"],
            ['A', "A"],
            ['B', "B"],
            ['C', "C"],
            ['D', "D"],
            ['E', "E"],
            ['F', "F"]
        ]);
        dd.setValue('0');
        this.appendDummyInput("")
            .appendField(dd, "flag");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_INFRARED_SIGNAL);
    }
};

/**
 * 发送红外线信号
 */
export const icb_actuator_send_infrared_signal = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SEND_INFRARED_SIGNAL);
        this.appendValueInput('s1');
        this.appendValueInput('s2');
        this.appendValueInput('s3');
        this.appendValueInput('s4');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SEND_INFRARED_SIGNAL);
    }
};

/**
* 设置内置电机速度
*/
export const icb_actuator_set_motor_speed = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_MOTOR_SPEED);
        this.appendValueInput('speed');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_MOTOR_SPEED);
    }
};

/**
 * 设置内置舵机角度
 */
export const icb_actuator_set_servo_angle = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_SERVO_ANGLE);
        this.appendValueInput('angle');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_SERVO_ANGLE);
    }
};

/**
 * 设置扩展舵机角度
 */
export const icb_actuator_set_ext_servo_angle = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_SERVO_ANGLE);
        this.appendValueInput('device');
        this.appendValueInput('angle')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_SERVO_ANGLE);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_SERVO_ANGLE);
    }
};

/**
 * 设置扩展电机速度
 */
export const icb_actuator_set_ext_motor_speed = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_MOTOR_SPEED);
        this.appendValueInput('device');
        this.appendValueInput('speed')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_MOTOR_SPEED);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_MOTOR_SPEED);
    }
};

/**
 * 设置扩展白灯
 */
export const icb_actuator_set_ext_light_white = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_LIGHT_WHITE);
        this.appendValueInput('device');
        this.appendValueInput('brightness')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_LIGHT_WHITE);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_LIGHT_WHITE);
    }
};

/**
 * 设置扩展红灯
 */
export const icb_actuator_set_ext_light_red = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_LIGHT_RED);
        this.appendValueInput('device');
        this.appendValueInput('brightness')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_LIGHT_RED);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_LIGHT_RED);
    }
};

/**
 * 设置扩展黄灯
 */
export const icb_actuator_set_ext_light_yellow = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_LIGHT_YELLOW);
        this.appendValueInput('device');
        this.appendValueInput('brightness')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_LIGHT_YELLOW);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_LIGHT_YELLOW);
    }
};


/**
 * 设置扩展绿灯
 */
export const icb_actuator_set_ext_light_green = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_LIGHT_GREEN);
        this.appendValueInput('device');
        this.appendValueInput('brightness')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_LIGHT_GREEN);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_LIGHT_GREEN);
    }
};

/**
 * 设置扩展交通灯菜单选项
 */
export const menu_icb_traffic_light_state = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        var dd = new Blockly.FieldDropdown([
            [Blockly.Msg.MIXLY_ESP32_ICB_TRAFFIC_LIGHT_CLOSED, "0x00"],
            [Blockly.Msg.MIXLY_ESP32_ICB_TRAFFIC_LIGHT_GREEN, "0x01"],
            [Blockly.Msg.MIXLY_ESP32_ICB_TRAFFIC_LIGHT_YELLOW, "0x02"],
            [Blockly.Msg.MIXLY_ESP32_ICB_TRAFFIC_LIGHT_RED, "0x04"]
        ]);
        dd.setValue('0');
        this.appendDummyInput("")
            .appendField(dd, "state");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_INFRARED_SIGNAL);
    }
};

/**
 * 设置扩展交通灯
 */
export const icb_actuator_set_ext_traffic_light_state = {
    init: function () {
        this.setColour(ACTUATOR_HUE);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SET_EXT_TRAFFIC_LIGHT_STATE);
        this.appendValueInput('device');
        this.appendValueInput('state')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_TRAFFIC_LIGHT_STATE);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SET_EXT_TRAFFIC_LIGHT_STATE);
    }
};