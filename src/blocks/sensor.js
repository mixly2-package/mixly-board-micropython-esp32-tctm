import * as Blockly from 'blockly/core';

const SENSOR_HUE = 40;  //'#9e77c9'//40;

/**
 * 扩展设备菜单选项
 */
export const menu_icb_ext_device = {
    init: function () {
        this.setColour(SENSOR_HUE);
        var dd = new Blockly.FieldDropdown([
            ["1", "0"],
            ["2", "1"],
            ["3", "2"],
            ["4", "3"]
        ]);
        dd.setValue('0');
        this.appendDummyInput("")
            .appendField(dd, "device");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_EXT_DEVICE_NO);
    }
};

/**
   * 获取扩展温度传感器值
   */
export const icb_sensor_ext_temperature = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_TEMPERATURE)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_TEMPERATURE);
    }
};

/**
   * 获取扩展湿度传感器值
   */
export const icb_sensor_ext_humidity = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_HUMIDITY)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_HUMIDITY);
    }
};

/**
   * 获取板载亮度传感器值，返回值0-4096
   */
export const icb_sensor_light = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.setOutput(true, Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_LIGHT);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_LIGHT);
    }
};

/**
   * 获取板载麦克风声音大小值，返回值0-4096
   */
export const icb_sensor_loudness = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.setOutput(true, Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_LOUDNESS);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_LOUDNESS);
    }
};

/**
   * 获取上一次接收到的红外命令，返回值0-0xffff
   */
export const icb_sensor_infrared_signal = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.setOutput(true, Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_INFRARED_SIGNAL);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_INFRARED_SIGNAL);
    }
};

/**
   * 获取扩展超声波传感器值
   */
export const icb_sensor_ext_ultrasonic = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_ULTRASONIC)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_ULTRASONIC);
    }
};

/**
 * 左右菜单选项
 */
export const menu_icb_left_right = {
    init: function () {
        this.setColour(SENSOR_HUE);
        var dd = new Blockly.FieldDropdown([
            [Blockly.Msg.MIXLY_ESP32_ICB_FLAG_LEFT, "1"],
            [Blockly.Msg.MIXLY_ESP32_ICB_FLAG_RIGHT, "2"]
        ]);
        dd.setValue('1');
        this.appendDummyInput("")
            .appendField(dd, "flag");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_LED_HUE);
    }
};

/**
   * 获取扩展巡线传感器值
   */
export const icb_sensor_ext_line_tracker = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_LINE_TRACKER)
            .setCheck(Number);
        this.appendValueInput('leftorright')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_SEPARATOR)
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_LINE_TRACKER);
    }
};

/**
   * 获取扩展旋钮值
   */
export const icb_sensor_ext_rotary_knob = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_ROTARY_KNOB)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_ROTARY_KNOB);
    }
};

/**
   * 获取扩展按钮值
   */
export const icb_sensor_ext_button = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_BUTTON)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_BUTTON);
    }
};

/**
   * 获取扩展人体移动传感器值
   */
export const icb_sensor_ext_body_move = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_BODY_MOVE)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_BODY_MOVE);
    }
};

/**
   * 获取扩展碰撞传感器
   */
export const icb_sensor_ext_impact = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_IMPACT)
            .setCheck(Number);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_IMPACT);
    }
};

/**
 * 颜色传感器属性菜单选项
 */
export const menu_icb_color_sensor_property = {
    init: function () {
        this.setColour(SENSOR_HUE);
        var dd = new Blockly.FieldDropdown([
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_CODE, "0"],
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_RED, "1"],
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_GREEN, "2"],
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_BLUE, "3"],
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_AMBIENT, "4"],
            [Blockly.Msg.MIXLY_ESP32_ICB_COLOR_SENSOR_PROPERTY_REFLECTED, "5"]
        ]);
        dd.setValue('0');
        this.appendDummyInput("")
            .appendField(dd, "property");
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_LED_HUE);
    }
};

/**
   * 获取扩展颜色传感器值
   */
export const icb_sensor_ext_color_sensor = {
    init: function () {
        this.setColour(SENSOR_HUE);
        this.appendValueInput('device')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_EXT_COLOR_SENSOR)
            .setCheck(Number);
        this.appendValueInput('property')
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_SEPARATOR)
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_ESP32_ICB_SENSOR_VALUE);
        this.setOutput(true, Number);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_ESP32_ICB_TOOLTIP_SENSOR_EXT_COLOR_SENSOR);
    }
};