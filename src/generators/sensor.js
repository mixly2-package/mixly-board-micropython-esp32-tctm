export const menu_icb_ext_device = function (_, generator) {
    var code = this.getFieldValue('device');
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_temperature = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x14;
    var device = offset + Number(device_no);

    var code = 'round(ex_device.sensor.GetExtTemperatureHumidity(' + device.toString() + ',1)/10,1)';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_humidity = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x14;
    var device = offset + Number(device_no);

    var code = 'round(ex_device.sensor.GetExtTemperatureHumidity(' + device.toString() + ',2)/10,1)';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_light = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';

    var code = 'tudao.get_brightness()';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_loudness = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';

    var code = 'tudao.get_soundlevel()';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_infrared_signal = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';

    var code = 'tudao.infrared.GetCmd()';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_ultrasonic = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x18;
    var device = offset + Number(device_no);

    var code = 'ex_device.sensor.GetExtUltrasonic(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const menu_icb_left_right = function (_, generator) {
    var code = this.getFieldValue('flag');
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_line_tracker = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x28;
    var device = offset + Number(device_no);
    var leftorright = generator.valueToCode(this, 'leftorright', generator.ORDER_ATOMIC);

    var code = 'ex_device.sensor.GetExtLineTracker(' + device.toString() + ',' + leftorright + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_rotary_knob = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x2c;
    var device = offset + Number(device_no);

    var code = 'ex_device.sensor.GetExtRotaryKnob(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_button = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x34;
    var device = offset + Number(device_no);

    var code = 'ex_device.sensor.GetExtButton(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_body_move = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x50;
    var device = offset + Number(device_no);

    var code = 'ex_device.sensor.GetExtBodyMoveSensor(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_impact = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x54;
    var device = offset + Number(device_no);

    var code = 'ex_device.sensor.GetExtImpactSensor(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const menu_icb_color_sensor_property = function (_, generator) {
    var code = this.getFieldValue('property');
    return [code, generator.ORDER_ATOMIC];
}

export const icb_sensor_ext_color_sensor = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';
    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x20;
    var device = offset + Number(device_no);
    var property = generator.valueToCode(this, 'property', generator.ORDER_ATOMIC);
    var cmd = '';

    switch (property) {
    case '0':
        cmd = 'Compound';
        break;
    case '1':
        cmd = 'Red';
        break;
    case '2':
        cmd = 'Green';
        break;
    case '3':
        cmd = 'Blue';
        break;
    case '4':
        cmd = 'Light';
        break;
    case '5':
        cmd = 'LightReflect';
        break;
    }

    var code = 'ex_device.sensor.GetExtColor' + cmd + '(' + device.toString() + ')';
    return [code, generator.ORDER_ATOMIC];
}

export const sensor_mpu9250_get_acceleration = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';
    var key = this.getFieldValue('key');
    var v = generator.valueToCode(this, 'SUB', generator.ORDER_ATOMIC);
    var code = 'tudao.' + v + '.mpu9250_get_' + key + '()';
    return [code, generator.ORDER_ATOMIC];
}