export const menu_icb_led_hue = function (_, generator) {
    var code = this.getFieldValue('flag');
    return [code, generator.ORDER_ATOMIC];
}

export const icb_actuator_led_hue = function (_, generator) {
    generator.definitions_['import_machine'] = 'import machine';
    generator.definitions_['import_neopixel'] = 'import neopixel';
    generator.definitions_['import_mixgo'] = 'import mixgo';
    var led = generator.valueToCode(this, 'led', generator.ORDER_ATOMIC);
    var hue = generator.valueToCode(this, 'hue', generator.ORDER_ATOMIC);
    var value_rvalue = 0;
    var value_gvalue = 0;
    var value_bvalue = 0;
    switch (hue) {
    case '0':
        value_rvalue = 0;
        value_gvalue = 0;
        value_bvalue = 0;
        break;
    case '1':
        value_rvalue = 255;
        value_gvalue = 0;
        value_bvalue = 0;
        break;
    case '2':
        value_rvalue = 255;
        value_gvalue = 165;
        value_bvalue = 0;
        break;
    case '3':
        value_rvalue = 255;
        value_gvalue = 255;
        value_bvalue = 0;
        break;
    case '4':
        value_rvalue = 0;
        value_gvalue = 128;
        value_bvalue = 0;
        break;
    case '5':
        value_rvalue = 0;
        value_gvalue = 255;
        value_bvalue = 255;
        break;
    case '6':
        value_rvalue = 0;
        value_gvalue = 0;
        value_bvalue = 255;
        break;
    case '7':
        value_rvalue = 128;
        value_gvalue = 0;
        value_bvalue = 128;
        break;
    }
    var code = 'mixgo.rgb[' + led + '] = (' + value_rvalue + ', ' + value_gvalue + ', ' + value_bvalue + ')\n';
    code += 'mixgo.rgb.write()\n';
    return code;
}

export const menu_icb_infrared_signal = function (_, generator) {
    var code = this.getFieldValue('flag');
    return [code, generator.ORDER_ATOMIC];
};

export const icb_actuator_send_infrared_signal = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';

    var s1 = generator.valueToCode(this, 's1', generator.ORDER_ATOMIC);
    var s2 = generator.valueToCode(this, 's2', generator.ORDER_ATOMIC);
    var s3 = generator.valueToCode(this, 's3', generator.ORDER_ATOMIC);
    var s4 = generator.valueToCode(this, 's4', generator.ORDER_ATOMIC);

    var code = 'tudao.infrared.Send(0,0x' + s1 + s2 + s3 + s4 + ')\n';
    return code;
}

export const icb_actuator_set_motor_speed = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';
    var speed = generator.valueToCode(this, 'speed', generator.ORDER_ATOMIC) || '0';
    var code = 'tudao.motor.Run(' + speed + ')\n';
    return code;
}

export const icb_actuator_set_servo_angle = function (_, generator) {
    generator.definitions_['import_tudao'] = 'import tudao';
    var angle = generator.valueToCode(this, 'angle', generator.ORDER_ATOMIC) || '0';
    var code = 'tudao.servo.Angle(' + angle + ')\n';
    return code;
}

export const icb_actuator_set_ext_servo_angle = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x1c;
    var device = offset + Number(device_no);
    var angle = generator.valueToCode(this, 'angle', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtServoAngle(' + device.toString() + ',' + angle + ')\n';
    return code;
}

export const icb_actuator_set_ext_motor_speed = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x30;
    var device = offset + Number(device_no);
    var speed = generator.valueToCode(this, 'speed', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtMotorSpeed(' + device.toString() + ',' + speed + ')\n';
    return code;
}

export const icb_actuator_set_ext_light_white = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x38;
    var device = offset + Number(device_no);
    var brightness = generator.valueToCode(this, 'brightness', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtLightWhite(' + device.toString() + ',' + brightness + ')\n';
    return code;
}

export const icb_actuator_set_ext_light_red = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x3c;
    var device = offset + Number(device_no);
    var brightness = generator.valueToCode(this, 'brightness', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtLightRed(' + device.toString() + ',' + brightness + ')\n';
    return code;
}

export const icb_actuator_set_ext_light_yellow = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x40;
    var device = offset + Number(device_no);
    var brightness = generator.valueToCode(this, 'brightness', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtLightYellow(' + device.toString() + ',' + brightness + ')\n';
    return code;
}

export const icb_actuator_set_ext_light_green = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x44;
    var device = offset + Number(device_no);
    var brightness = generator.valueToCode(this, 'brightness', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtLightGreen(' + device.toString() + ',' + brightness + ')\n';
    return code;
}

export const menu_icb_traffic_light_state = function (_, generator) {
    var code = this.getFieldValue('state');
    return [code, generator.ORDER_ATOMIC];
}

export const icb_actuator_set_ext_traffic_light_state = function (_, generator) {
    generator.definitions_['import_ex_device'] = 'import ex_device';

    var device_no = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC);
    var offset = 0x48;
    var device = offset + Number(device_no);
    var state = generator.valueToCode(this, 'state', generator.ORDER_ATOMIC) || '0';

    var code = 'ex_device.icb.SetExtTrafficLight(' + device.toString() + ',' + state + ')\n';
    return code;
}